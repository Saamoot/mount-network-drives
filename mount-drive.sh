#!/usr/bin/env sh

usage() {
  echo "Variables:"
  echo "- MOUNT_USER"
  echo "- MOUNT_GROUP"
  echo "- MOUNT_PERMISSIONS"
  echo ""
  echo "Usage:"
  echo "$ $(basename "${0}") {authFile} {networkPath} {mountFile}"
  echo "$ $(basename "${0}") ~/.secret/auth //1.2.3.4/public /media/public"
  echo "$ MY_USER=\"guest\" MY_GROUP=\"adm\" $(basename "${0}") ~/.secret/auth //1.2.3.4/public /media/public"
  echo ""
  echo "AuthFile contents:"
  echo "username={my-user}"
  echo "password={my-password}"
  echo ""
}

if [ "${#}" -le 0 ]; then
  usage
  exit 0
fi


. ./common.sh

SUDO=""
if [ "root" != "$(whoami)" ]; then
  SUDO="sudo"
fi

authFile="${1}"
networkPath="${2}"
mountFile="${3}"

MOUNT_USER="${MOUNT_USER:-$(id -i)}"
MOUNT_GROUP="${MOUNT_GROUP:-$(id -g)}"
MOUNT_PERMISSIONS="${MOUNT_PERMISSIONS:-"0755"}"

sudo mkdir -p "$mountFile"

mountFileContent="$(ls "$mountFile")"
if [ -n "$mountFileContent" ]; then
  myEcho
  myEcho "Directory \"$mountFile\" already contains files, \"$networkPath\" is already mounted or directory is not empty"
  myEcho
  return 0
fi

myEcho
myEcho "Mounting \"$networkPath\" to dir \"$mountFile\""
$SUDO mount \
  -t cifs \
  -o credentials="${authFile}",dir_mode="${MOUNT_PERMISSIONS}",uid="${MOUNT_USER}",gid="${MOUNT_GROUP}",vers=3.0 \
  "${networkPath}" \
  "${mountFile}"

myEcho