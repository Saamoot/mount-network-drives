#!/usr/bin/env sh

. ./common.sh

MOUNT_DELIMITER="${MOUNT_DELIMITER:-";"}"
mountsListFile="./.mounts-list"

while IFS='' read -r line || [ -n "${line}" ]; do
  echo "line: ${line}"
  authFile="$(echo "${line}" | cut -d "${MOUNT_DELIMITER}" -f 1)"
  networkFolder="$(echo "${line}" | cut -d "${MOUNT_DELIMITER}" -f 2)"
  mountFolder="$(echo "${line}" | cut -d "${MOUNT_DELIMITER}" -f 3)"

  ./mount-drive "${authFile}" "${networkFolder}" "${mountFolder}"

done < "${mountsListFile}"