#!/usr/bin/env sh

. ./common.sh

authFile="${1:-}"
networkPath="${2:-}"
mountFile="${3:-}"

if [ -z "${authFile}" ]; then
  myEcho "Set path to authentication file:"
  read -r authFile
fi

if [ -z "${networkPath}" ]; then
  myEcho "Set path to folder on network:"
  read -r networkPath
fi

if [ -z "${mountFile}" ]; then
  myEcho "Set path where network folder should be mounted:"
  read -r mountFile
fi

errors=""
if [ ! -f "${authFile}" ]; then
  errors="${errors}\n- Could not find authentication file \"${authFile}\""
fi
if [ ! -d "${mountFile}" ]; then
  errors="${errors}\n- Could not find mount folder \"${mountFile}\""
fi

if [ -n "${errors}" ]; then
  myEcho "Following error(s) occurred: ${errors}"
  exit  1
fi

MOUNT_DELIMITER="${MOUNT_DELIMITER:-";"}"
mountsListFile="./.mounts-list"
mountsListLine="${authFile}${MOUNT_DELIMITER}${networkPath}${MOUNT_DELIMITER}${mountFile}"

echo "${mountsListLine}" >> "${mountsListFile}"