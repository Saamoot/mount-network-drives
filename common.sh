#!/usr/bin/env sh

myEcho() {
  message="${1}"

  VERBOSE="${VERBOSE:-}"
  VERBOSE="${VERBOSE#-}"
  VERBOSE="${VERBOSE#-}"

  if [ "v" = "${VERBOSE}" ]; then
    echo "${0} ${message}"
    echo "${message}"
    return 0
  fi

  if [ "q" = "${VERBOSE}" ]; then
    return 0
  fi

  echo "${message}"
  return 0
}