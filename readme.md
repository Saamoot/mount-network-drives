# Usage
- clone repository
- change to cloned directory

## single use / one time use
```shell
$ mount-drive.sh ~/.screts/network-disk //192.168.144.120/share /mnt/network-disk-share
```
## repeated usage
- add mount-drive(s)
```shell
# non-interactive
./add-mount-drive.sh ~/.screts/network-disk-alfa //192.168.144.120/share /mnt/alfa-share
./add-mount-drive.sh ~/.screts/network-disk-beta //192.168.144.121/share /mnt/beta-share

# interactive
./add-mount-drive.sh
```
- mount added drives
```shell
./mount-all-drives.sh
```